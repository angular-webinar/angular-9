import { Injectable } from '@angular/core';

console.log('ServiceOne service will be used by both the components')

@Injectable({
  providedIn: 'root'
})
export class ServiceOneService {

  constructor() { 
    console.log('service one instantiated')
  }
}
