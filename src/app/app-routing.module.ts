import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';


const routes: Routes = [
  {
    path: '',
    component: WelcomeComponent
  },
  {
    path: "feature-one",
    loadChildren: './feature-one/feature-one.module#FeatureOneModule'
  },
  {
    path: "feature-two",
    loadChildren: './feature-two/feature-two.module#FeatureTwoModule'
  },
  {
    path: "feature-three",
    loadChildren: './f3/f3.module#FeatureThreeModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
