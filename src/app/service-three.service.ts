import { Injectable } from '@angular/core';
console.log("Service three will be bundled even though not used..")
@Injectable({
  providedIn: 'root'
})
export class ServiceThreeService {

  constructor() {
    console.log('service three instantiated..')
   }
}
