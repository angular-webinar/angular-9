import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureTwoComponent } from './feature-two/feature-two.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [FeatureTwoComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path:'', component: FeatureTwoComponent}
    ])
  ]
})
export class FeatureTwoModule { }
