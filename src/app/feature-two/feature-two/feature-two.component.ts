import { Component, OnInit } from '@angular/core';
import { ServiceOneService } from 'src/app/service-one.service';

@Component({
  selector: 'app-feature-two',
  templateUrl: './feature-two.component.html',
  styleUrls: ['./feature-two.component.css']
})
export class FeatureTwoComponent implements OnInit {

  constructor(private serviceOne:ServiceOneService) { }

  ngOnInit(): void {
    console.log('Feature two component initalized')
  }

}
