import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceThreeService } from './service-three.service';
import { WelcomeComponent } from './welcome/welcome.component';
import { FeatureOneModule } from './feature-one/feature-one.module';
import { FeatureTwoModule } from './feature-two/feature-two.module';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FeatureOneModule,
    FeatureTwoModule
  ],
  providers: [ServiceThreeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
