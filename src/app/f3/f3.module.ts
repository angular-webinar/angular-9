import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureThreeComponent } from './feature-three/feature-three.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [FeatureThreeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: '', component: FeatureThreeComponent}
    ])
  ]
})
export class F3Module { }
