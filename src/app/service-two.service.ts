import { Injectable } from '@angular/core';

console.log('service two will not be part of the bundle')

@Injectable({
  providedIn: 'root'
})
export class ServiceTwoService {

  constructor() {
    console.log('service two instantiated')
   }
}
