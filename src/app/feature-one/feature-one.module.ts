import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureOneComponent } from './feature-one/feature-one.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [FeatureOneComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: '', component: FeatureOneComponent}
    ])
  ]
})
export class FeatureOneModule { }
