import { Component, OnInit } from '@angular/core';
import { ServiceOneService } from 'src/app/service-one.service';

@Component({
  selector: 'app-feature-one',
  templateUrl: './feature-one.component.html',
  styleUrls: ['./feature-one.component.css']
})
export class FeatureOneComponent implements OnInit {

  constructor(private serviceOne:ServiceOneService) { }

  ngOnInit(): void {
    console.log('Feature one component instantiated..')
  }

}
